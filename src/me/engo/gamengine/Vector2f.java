package me.engo.gamengine;

public class Vector2f
{
    private float x;

    public float getX()
    {
        return x;
    }

    public void setX(float x)
    {
        this.x = x;
    }

    public float getY()
    {
        return y;
    }

    public void setY(float y)
    {
        this.y = y;
    }

    private float y;

    public Vector2f(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public String toString()
    {
        return "(" + x + " " + y + ")";
    }
}
